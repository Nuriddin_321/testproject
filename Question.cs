using System.Collections.Generic;

public class Question
{

    public string Content { get; set; }
    public Dictionary<EOption, string> Options { get; set; }
    private EOption Answer { get; set; }

    public Question(string content, Dictionary<EOption, string> options, EOption answer)
    {
        Content = content;
        Options = options;
        Answer = answer;
    }

    public bool IsCorrect(EOption option)
    {
       return Answer == option;
    }

    public override string ToString()
    {   
        string temp = @"";
        foreach (var value in Options)
        {
            temp += $"\n{value.Key}  {value.Value} ";
        }
        string result = $@"{Content} {temp}";
        return result;
    }

}