﻿

using System.Linq;
using System.Net.Mime;
using System.Collections.Generic;
using System;

class Program
{
    public static void Main(string[] args)
    {

        var answerPath = Directory.GetCurrentDirectory();
        var answerFile = Path.Combine(answerPath, "testAnswers.txt");
        using var str = File.OpenText(answerFile);
        List<string> listAnswers = new List<string>();
        string textLine = "";
        while ((textLine = str.ReadLine()) != null)
        {
            listAnswers.Add(textLine);
        }

        var testPath = Directory.GetCurrentDirectory();
        var file = Path.Combine(testPath, "test.txt");
        using var sr = File.OpenText(file);
        string text = "";

        List<string> listTxtCopy = new List<string>();
        while ((text = sr.ReadLine()) != null)
        {
            listTxtCopy.Add(text);
        }

        List<Question> questionList = new List<Question>();
        int indexOfAnswer = 0;

        for (int i = 0; i < listTxtCopy.Count; i++)
        {
            string word = listTxtCopy[i];
            if (char.IsDigit(word[0]))
            {
                string questionContent = listTxtCopy[i];
                Dictionary<EOption, string> dictionaryOpstions = new Dictionary<EOption, string>();
                int numberEnum = 0;
                var arrEnums = Enum.GetValues<EOption>();
                for (int j = i + 1; j < listTxtCopy.Count - 1; j++)
                {
                    if (char.IsDigit(listTxtCopy[j][0]))
                    {
                        break;
                    }
                    else
                    {
                        string options = listTxtCopy[j];
                        dictionaryOpstions.Add(arrEnums[numberEnum], options);
                        numberEnum++;
                    }
                }
 
                if (listAnswers[indexOfAnswer].Length > 3)
                {
                    // some code here
                    // rightAnswer = EOption.A | EOption.B;
                }
                else
                {
                    var charListAnswer = listAnswers[indexOfAnswer].First(x => char.IsLetter(x));
                    foreach (var item in arrEnums)
                    {
                        if (charListAnswer == item.ToString()[0])
                        {
                          EOption rightAnswer = item;
                          Question question = new Question(questionContent, dictionaryOpstions, rightAnswer);
                          questionList.Add(question); 
                        }
                    }
                }

            }
        }
        //Question class obyektiga test.txtdan savol va variantlar ko'chirib olindi

        Test test = new Test("Dasturlash", questionList, 60);
        test.TakeTest();

    }
}


